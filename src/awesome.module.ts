import { NgModule, ModuleWithProviders } from '@angular/core';
import { AwesomeComponent } from './components/awesome.component';
import { AwesomeProvider } from './providers/awesome.provider';
import { IonicModule } from 'ionic-angular';

@NgModule({
    imports: [
        IonicModule
    ],
    declarations: [
        AwesomeComponent
    ],
    exports: [
        AwesomeComponent
    ],
})
export class AwesomeModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AwesomeModule,
            providers: [AwesomeProvider]
        };
    }
}