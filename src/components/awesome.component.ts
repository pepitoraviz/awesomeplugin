import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
const TEMPLATE = `
<ion-header>
    <ion-navbar color="light">
        <ion-title>
           Awesome Page
        </ion-title>
    </ion-navbar>
</ion-header>

<ion-content padding>

    <p>Welcome to awesome page bruh</p>
    <button ion-button round (click)="leavePage()">Take me back boi</button>
</ion-content>
`
const CSS = `
.special-text {
    font-weight: 800;
    font-size: 15pt;
    text-align: center;
    color: #0000FF;
}
`
@Component({
    moduleId: module.id + "",
    selector: 'awesome-component',
    template: TEMPLATE,
    styles: [CSS]
})
export class AwesomeComponent {
    constructor(private navCtrl: NavController) { }

    leavePage() {
        this.navCtrl.pop();
    }
}